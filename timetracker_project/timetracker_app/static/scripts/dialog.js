﻿function showModalByID(ID) {
    var element = document.getElementById(ID);
    showModal(element);
}

function showModal(element) {
    element.className = 'modal fade show';
    element.style.paddingTop = '50px';
    element.style.backgroundColor = 'rgba(128, 128, 128, 0.5)';
    element.style.opacity = 1;

    let buttonsList = element.querySelectorAll('[data-dismiss="modal"]');
    buttonsList.forEach(function (button) {
        if (button.dataset.dismiss == 'modal') {
            button.dataset.dismiss = 'none';
            button.addEventListener('click', function () {
                hideModal(element);
            });
        }
    });
}

function hideModal(element) {
    element.className = 'modal fade';
}